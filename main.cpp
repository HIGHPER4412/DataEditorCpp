#include <iostream>
#include <vector>
#include <fstream>
#include <string>

using namespace std;

class editF {
  private:
    string AddtoFStr;
    string ChangeStr;
    string changerFunStr;
    int i = 1;
    int changeInt;
    string removerStr;
    int removerInt;
  protected:
    vector<string> storeArrString;
    void Removed(fstream& mainUFile, string filename)
    {
      mainUFile.open(filename, ios::in);
      while(getline(mainUFile, removerStr))
      {
        storeArrString.push_back(removerStr);
        cout << i << ": " << removerStr << endl;
        i++;
      }
      mainUFile.close();
      if(storeArrString.size() != 0) {
        cout << "Choose a N to Remove it: ";
        cin >> removerInt;
        storeArrString.erase(storeArrString.begin()+(removerInt-1));
        mainUFile.open(filename, ios::out);
        for(int j = 0; j < storeArrString.size(); j++)
        {
          mainUFile << storeArrString.at(i);
        }
        mainUFile.close();
      } else {
        cout << "File is Empty! cannot Remove" << endl;
      }
      i = 1;
      storeArrString.clear();
    }

    void AddtoF(fstream& mainUFile, string filename)
    {
      mainUFile.open(filename, ios::app);
      cout << "Enter a String: ";
      cin.ignore(1,'\n');
      getline(cin, AddtoFStr);
      mainUFile << AddtoFStr << "\n";
      mainUFile.close();
    }

    void Change(fstream& mainUFile, string filename)
    {
      mainUFile.open(filename, ios::in);
      while(getline(mainUFile, ChangeStr))
      {
        storeArrString.push_back(ChangeStr);
        cout << i << ": " << ChangeStr << endl;
        i++;
      }
      mainUFile.close();
      cout << "Choose N Want to Change: ";
      cin >> changeInt;
      cout << storeArrString.at(changeInt - 1) << " Change To ";
      cin.ignore(1,'\n');
      getline(cin, changerFunStr); 
      storeArrString.at(changeInt - 1) = changerFunStr;
      mainUFile.open(filename, ios::out);
      for(int j = 0; j < storeArrString.size(); j++)
      {
        mainUFile << storeArrString[j];
        mainUFile << endl;
        //cout << storeArrString.at(0) << endl; 
      }
      i = 1;
      storeArrString.clear();
      mainUFile.close();
    }
    void Remove(fstream& mainUFile, string filename)
    {

    }
};

class FileUser : public editF {
  private:
    fstream mainUFile;
    string fileName = "MainF.txt";
    string DataShow;
    int chooseEdit;
  public:
    bool start = true;
    void Open()
    {
      mainUFile.open(fileName, ios::in);
      if(!mainUFile)
      {
        cout << "Error File Can't Open!" << endl;
      } else {
        cout << "File Opened" << endl; 
      }
    }
    void Edit()
    {
      if(!mainUFile.is_open())
      {
        cout << "File Never Been Opened!" << endl;
      } else {
        mainUFile.close();
        cout << "SuccessFull To Editing" << endl;
        cout << "(1) Change" << endl;
        cout << "(2) Remove" << endl;
        cout << "(3) Add a Data/Text" << endl;
        cout << "Choose a Operation: ";
        cin >> chooseEdit;
        switch (chooseEdit) {
          case 1:
            Change(mainUFile, fileName);
            break;
          case 2:
            Removed(mainUFile, fileName);
            break;
          case 3:
            AddtoF(mainUFile, fileName);   
            break;
          default:
            cout << "It's Not on Chooses" << endl;
            break;
        }
        mainUFile.open(fileName);
      }
    }

    void Search()
    {
      cout << "This is Search" << endl;
    }

    void Show()
    {
      if(!mainUFile.is_open())
      {
        cout << "File Never Been Opened!" << endl;
      } else {
        mainUFile.close();
        //Open as Reader
        mainUFile.open(fileName, ios::in);
        cout << "Show the data of File" << endl;
        cout << endl;
        while(getline(mainUFile, DataShow))
        {
          cout << DataShow << endl; //bug
        }
        cout << endl;
        mainUFile.close();
        //Then open Again if the user want to edit his data
        mainUFile.open(fileName);
      }
    }
    void Exit()
    {
      start = false;
      cout << "File Has Been Saved" << endl;
      mainUFile.close();
    }
};

int main()
{
  FileUser FileU;
  int usrInput;
  while(FileU.start)
  {
    cout << "(1) Open" << endl;
    cout << "(2) Edit" << endl;
    cout << "(3) Search" << endl;
    cout << "(4) Show All in A File" << endl;
    cout << "(5) Exit" << endl;
    cout << "Choose an Operation: ";
    cin >> usrInput;
    switch (usrInput) {
      case 1:
        FileU.Open();
        break;
      case 2:
        FileU.Edit();
        break;
      case 3:
        FileU.Search();
        break;
      case 4:
        FileU.Show();
        break;
      case 5:
        FileU.Exit();
        break;
      default:
        cout << "Please Choose 1 - 5" << endl;
        break;
    }
  }
  return 0;
}

